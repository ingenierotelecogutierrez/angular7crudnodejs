const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require ('./db');

const businessRoute = require('./routes/business.route');
const usersRoute = require('./routes/users.route');


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/ng7crud', { useNewUrlParser:true, useUnifiedTopology: true})
    .then( db => {
        console.log('Database is connected');
    })
    .catch( error => {
        console.log(error);
    });

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.use('/business', businessRoute);
app.use('/users', usersRoute);


let port = process.env.PORT || 3000;

const server = app.listen(3000, () => {
    console.log(`Server listening on ${port}`);
})