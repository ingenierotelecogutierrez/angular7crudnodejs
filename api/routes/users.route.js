const express = require('express');
const app = express();
const usersRoutes = express.Router();

let Users = require('../models/Users');

//Store route
usersRoutes.route('/add').post( async (req, res) => {
    let users = new Users(req.body);
    await users.save()
        .then( data => {
            res.status(200).json({'users': 'user in added successfully'});
        })
        .catch( error => {
            res.status(400).send('unable to save to database');
        })
})

usersRoutes.route('/').get( (req, res) => {
    Users.find( (err, users) => {
        if (err){
            console.log(err);
        }else{
            res.json(users);
        }
    })
})

// Defined edit route
usersRoutes.route('/edit/:id').get(function (req, res) {
    let id = req.params.id;
    Users.findById(id, function (err, users){
        res.json(users);
    });
});

//  Defined update route
usersRoutes.route('/update/:id').post(function (req, res) {
   Users.findById(req.params.id, function(err, user) {
        if (!user)
            return (new Error('Could not load Document'));
        else {
            user.name = req.body.name;
            user.surname = req.body.surname;
            user.email = req.body.email;
            user.tlf = req.body.tlf;

            user.save()
            .then(users => {
                res.json('Update complete');
            })
            .catch(err => {
                res.status(400).send("unable to update the database");
            });
        }
    });
});

// Defined delete | remove | destroy route
usersRoutes.route('/delete/:id').get(function (req, res) {
    Users.findByIdAndRemove({_id: req.params.id}, function(err, users){
        if(err) {
            res.json(err);
        }else{
            Users.find( (err, userses) => {
                if (err){
                    console.log(err);
                }else{
                    res.json(userses);
                }
            })
        }
    });
});

module.exports = usersRoutes;
  