const express = require('express');
const app = express();
const businessRoutes = express.Router();

let Business = require('../models/Business');

//Store route
businessRoutes.route('/add').post( async (req, res) => {
    console.log('GUAU');
    let business = new Business(req.body);
    console.log('GUAU');
    await business.save()
        .then( business => {
            res.status(200).json({'business': 'business in added successfully'});
        })
        .catch( error => {
            res.status(400).send('unable to save to database');
        })
})

businessRoutes.route('/').get( (req, res) => {
    Business.find( (err, businesses) => {
        if (err){
            console.log(err);
        }else{
            res.json(businesses);
        }
    })
})

// Defined edit route
businessRoutes.route('/edit/:id').get(function (req, res) {
    let id = req.params.id;
    console.log(id);
    Business.findById(id, function (err, business){
        res.json(business);
    });
});

//  Defined update route
businessRoutes.route('/update/:id').post(function (req, res) {
    Business.findById(req.params.id, function(err, business) {
        if (!business)
            return (new Error('Could not load Document'));
        else {
            business.person_name = req.body.person_name;
            business.business_name = req.body.business_name;
            business.business_gst_number = req.body.business_gst_number;

            business.save()
            .then(business => {
                res.json('Update complete');
            })
            .catch(err => {
                res.status(400).send("unable to update the database");
            });
        }
    });
});

// Defined delete | remove | destroy route
businessRoutes.route('/delete/:id').get(function (req, res) {
    Business.findByIdAndRemove({_id: req.params.id}, function(err, business){
        if(err) {
            res.json(err);
        }else{
            Business.find( (err, businesses) => {
                if (err){
                    console.log(err);
                }else{
                    res.json(businesses);
                }
            })
        }
    });
});

module.exports = businessRoutes;
  