const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Users = new Schema({
    name : {
        type : String
    },
    surname : {
        type : String
    },
    email : {
        type : String
    },
    tlf : {
        type : Number
    }
},{
    collection : 'users'
})

module.exports = mongoose.model('Users', Users);