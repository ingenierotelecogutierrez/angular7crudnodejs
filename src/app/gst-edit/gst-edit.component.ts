import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessService } from '../business.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-gst-edit',
  templateUrl: './gst-edit.component.html',
  styleUrls: ['./gst-edit.component.scss']
})
export class GstEditComponent implements OnInit {

  business;
  angForm : FormGroup;

  constructor(private bs: BusinessService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder) {
      this.createForm();
      this.route.params.subscribe( params => {
        this.bs.getToEditBusiness(params['id']).subscribe( res => {
          this.angForm.controls['id'].patchValue(res['_id']);
          this.angForm.controls['person_name'].patchValue(res['person_name']);
          this.angForm.controls['business_name'].patchValue(res['business_name']);
          this.angForm.controls['business_gst_number'].patchValue(res['business_gst_number']);
        })
      })
     }

  ngOnInit() {
    
  }

  createForm(){
    this.angForm = this.fb.group({
      id: [''],
      person_name: ['', Validators.required ],
      business_name: ['', Validators.required ],
      business_gst_number: ['', Validators.required ]
    })
  }

  updateBusiness(id){
    this.bs.updateBusiness(id, this.angForm.value.person_name, this.angForm.value.business_name, this.angForm.value.business_gst_number).subscribe( res => {
      this.router.navigate(['/business']);
    })
  }

}
