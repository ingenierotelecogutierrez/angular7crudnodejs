import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  uri = 'http://localhost:3000/business';

  constructor(private http: HttpClient) { }

  addBusiness(person_name, business_name, business_gst_number){
    const obj = {
      person_name : person_name,
      business_name : business_name,
      business_gst_number : business_gst_number
    };

    return this.http.post(`${this.uri}/add`, obj);
      
  }

  getBusiness(){
    return this.http.get(`${this.uri}/`);
  }

  getToEditBusiness(id){
    return this.http.get(`${this.uri}/edit/${id}`);
  }

  updateBusiness(id, person_name, business_name, business_gst_number){
    const obj = {
      person_name : person_name,
      business_name : business_name,
      business_gst_number : business_gst_number
    };

    return this.http.post(`${this.uri}/update/${id}`, obj);
  }

  deleteBusiness(id){
    return this.http.get(`${this.uri}/delete/${id}`);
  }
}
