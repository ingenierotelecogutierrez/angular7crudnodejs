import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../users.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  user;
  angForm : FormGroup;

  constructor(private bs: UsersService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder) {
      this.createForm();
      this.route.params.subscribe( params => {
        this.bs.getToEditUser(params['id']).subscribe( res => {
          this.angForm.controls['id'].patchValue(res['_id']);
          this.angForm.controls['name'].patchValue(res['name']);
          this.angForm.controls['surname'].patchValue(res['surname']);
          this.angForm.controls['email'].patchValue(res['email']);
          this.angForm.controls['tlf'].patchValue(res['tlf']);
        })
      })
  }

  ngOnInit() {
  }

  createForm(){
    this.angForm = this.fb.group({
      id: [''],
      name: ['', Validators.required ],
      surname: ['', Validators.required ],
      email: ['', Validators.required ],
      tlf: ['', Validators.required ]
    })
  }

  updateUser(id){
    this.bs.updateUser(id, this.angForm.value.name, this.angForm.value.surname, this.angForm.value.email, this.angForm.value.tlf).subscribe( res => {
      this.router.navigate(['/users']);
    })
  }
}
