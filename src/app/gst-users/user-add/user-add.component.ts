import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  angForm : FormGroup;

  constructor(private fb: FormBuilder, 
    private bs : UsersService, 
    private router: Router) {
    this.createForm();
  }

  createForm(){
    this.angForm = this.fb.group({
      name  : ['', Validators.required],
      surname : ['', Validators.required],
      email : ['', Validators.required],
      tlf : ['', Validators.required]
    });
  }

  addUser(){
    this.bs.addUser(this.angForm.value.name, this.angForm.value.surname, this.angForm.value.email, this.angForm.value.tlf)
      .subscribe( res => { 
        this.router.navigate(['/users']);
      })
  }

  ngOnInit() {
  }

}
