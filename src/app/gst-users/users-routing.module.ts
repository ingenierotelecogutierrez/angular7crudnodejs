import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GstUsersComponent } from './gst-users.component';
import { UserAddComponent } from './user-add/user-add.component';
import { UserEditComponent } from './user-edit/user-edit.component';


const routes: Routes = [
  {path: 'users', component: GstUsersComponent},
  {path: 'users/create', component: UserAddComponent},
  {path: 'users/edit/:id', component: UserEditComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
