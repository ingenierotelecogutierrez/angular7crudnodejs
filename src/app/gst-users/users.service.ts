import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  uri = 'http://localhost:3000/users';

  constructor(private http: HttpClient) { }

  addUser(name, surname, email, tlf){
    const obj = {
      name : name,
      surname : surname,
      email : email,
      tlf : tlf
    };

    return this.http.post(`${this.uri}/add`, obj);
      
  }

  getUsers(){
    return this.http.get(`${this.uri}/`);
  }

  getToEditUser(id){
    return this.http.get(`${this.uri}/edit/${id}`);
  }

  updateUser(id, name, surname, email, tlf){
    const obj = {
      name : name,
      surname : surname,
      email : email,
      tlf : tlf
    };

    return this.http.post(`${this.uri}/update/${id}`, obj);
  }

  deleteUser(id){
    return this.http.get(`${this.uri}/delete/${id}`);
  }
}
