import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'app-gst-users',
  templateUrl: './gst-users.component.html',
  styleUrls: ['./gst-users.component.scss']
})
export class GstUsersComponent implements OnInit {

  constructor(private bs: UsersService) { }

  users;

  ngOnInit() {
    this.bs.getUsers().subscribe( res => {
      this.users = res;
    })
  }

  deleteUser(id){
    this.bs.deleteUser(id).subscribe( res => {
        this.users = res;
    })
  }

}
