import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';

import { UsersRoutingModule } from './users-routing.module';

import { HttpClientModule } from '@angular/common/http';

import { UsersService } from './users.service';
import { GstUsersComponent } from './gst-users.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserAddComponent } from './user-add/user-add.component';

@NgModule({
  declarations: [
    GstUsersComponent,
    UserEditComponent,
    UserAddComponent
  ],
  imports: [
    BrowserModule,
    UsersRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    UsersService
  ],
  bootstrap: [GstUsersComponent]
})
export class UsersModule { }
