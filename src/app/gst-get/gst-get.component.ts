import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessService } from '../business.service';

@Component({
  selector: 'app-gst-get',
  templateUrl: './gst-get.component.html',
  styleUrls: ['./gst-get.component.scss']
})
export class GstGetComponent implements OnInit {

  businesses;

  constructor(private bs: BusinessService, private router: Router) { }

  ngOnInit() {
    this.bs.getBusiness().subscribe( res => {
      this.businesses = res;
    })
  }

  deleteBusiness(id){
    this.bs.deleteBusiness(id).subscribe( res => {
        this.businesses = res;
    })
  }

}
