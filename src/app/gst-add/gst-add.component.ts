import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { BusinessService } from '../business.service';

@Component({
  selector: 'app-gst-add',
  templateUrl: './gst-add.component.html',
  styleUrls: ['./gst-add.component.scss']
})
export class GstAddComponent implements OnInit {

  angForm : FormGroup;

  constructor(private fb: FormBuilder, 
    private bs : BusinessService, 
    private router: Router) {
    this.createForm();
  }

  createForm(){
    this.angForm = this.fb.group({
      person_name  : ['', Validators.required],
      business_name : ['', Validators.required],
      business_gst_number : ['', Validators.required]
    });
  }

  addBusiness(){
    this.bs.addBusiness(this.angForm.value.person_name, this.angForm.value.business_name, this.angForm.value.business_gst_number)
      .subscribe( res => { 
        this.router.navigate(['/business']);
      })
  }

  ngOnInit() {
  }

}
